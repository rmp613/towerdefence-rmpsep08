﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class BasicEnemy : BaseHealth {
	public float moveSpeed;
	//armor,regen,other special properties, targets,end bases,tower lists,	players, attack method, switch ai
	[SerializeField]
	protected float weaponDamage, weaponRange, offsetToUpdatePath; //offsettopdatepath is how far the current target is away from the end of the current path in order to get a new path
	protected Pathfinder pathfinder;
	public enum targetStateEnum{targetBase,targetPlayer,targetTurret,attackingBase}
	protected targetStateEnum state = targetStateEnum.targetBase;
	protected int currentPathIndex = 0;
	protected int pathAheadCheck = 5;
	protected List<Vector2> path;
	private Vector3 moveTarget;
	protected GameObject endBase;
	protected int enemyId;
	GameObject currentTarget;
	Vector3 targetOldPos;


	// Use this for initialization
	//@Override 
	public void Start() {
		pathfinder = new Pathfinder();
		endBase = GameObject.Find ("Base");
		updateState ();
		updatePath ();
		Physics.IgnoreCollision (GetComponent<BoxCollider> (), GameObject.FindGameObjectWithTag ("InWall").GetComponent<BoxCollider>());
	}

	// Update is called once per frame
	protected void Update () {
		updateState ();
		stateActions ();
		//if player moves or (add later if a tower is placed down) update path
		if (state == targetStateEnum.targetPlayer) {
			if (Vector3.Distance(currentTarget.transform.position, targetOldPos) > offsetToUpdatePath) {
				updatePath ();
			}
			followPath ();
			targetOldPos = currentTarget.transform.position;
		}
	}
	protected void OnCollisionEnter(Collision collision){
		updatePath ();
	}
	//COULDNT GET THIS TO WORK AS THEY RAN INTO WALLS AND GOT STUCK SOMETIMES ALTHOUGHT THE OTHER CODE DOES THE SAME SO NOT SURE WHAT TO DO HERE
	//also if you want to try this and cant figure out the variables i havent included here i have a vs backed up so ask me and ill send u how i had it set up
/*	void LookAhead(){
		bool wayClear = true;
		lookIndex = currentPathIndex;
		RaycastHit hitObstacle;
		Vector3 prevTarget = Grid.GetWorldPos((int)path[currentPathIndex].x, (int)path[currentPathIndex].y);;
		//while there are no obstacles between the current pathpoint and the pathpoint we are looking ahead to...
		while (wayClear) {
			//get movetarget
			moveTarget = Grid.GetWorldPos((int)path[lookIndex].x, (int)path[lookIndex].y);
			moveTarget = new Vector3 (moveTarget.x, transform.position.y, moveTarget.z); //make movetarget.y the same as enemypos.y
			//setup the ray checking at foot level of enemies for obstacles ahead
			Vector3 rayOrigin = new Vector3 (transform.position.x, transform.position.y - height / 3, transform.position.z);
			Vector3 rayTarget = new Vector3 (moveTarget.x, transform.position.y - height / 3, moveTarget.z);
			Vector3 rayDir = rayTarget - rayOrigin;
			rayDir.Normalize ();

			//if obstacle ahead change movetarget to previous movetarget, change currentPathIndex to lookindex and return
			if (Physics.Raycast (rayOrigin, rayDir, out hitObstacle, Vector3.Distance (rayTarget, rayOrigin))) {
				moveTarget = prevTarget;
				currentPathIndex = lookIndex-1;
				wayClear = false;
				return;
			}


			//else increment lookindex and set prevTarget
			lookIndex++;
			prevTarget = moveTarget;
		}
	}*/
	virtual protected void updateState(){
		if (state != targetStateEnum.targetPlayer) {
			if (state != targetStateEnum.attackingBase && Vector3.Distance (endBase.transform.position, transform.position) < weaponRange) {
				state = targetStateEnum.attackingBase;
			} else {
				state = targetStateEnum.targetBase;
			}
		}
	}
	virtual protected void stateActions(){
		switch (state) {
		case targetStateEnum.targetBase:
			break;
		case targetStateEnum.attackingBase:
			
			Explode ();

			break;
		}
	}

	protected void Explode() {
		if(!GetComponent<ParticleSystem>().isPlaying){
			endBase.GetComponent<Base>().RpcTakeDamage(weaponDamage);
		}
		var exp = GetComponent<ParticleSystem>();
		exp.Play();
		Destroy(gameObject, exp.duration);

		//anything else?
		//checking if it works with wave
		//how do i do a wave?


	}
	public void setAttributes(float moveSpeed, float health, float weaponDamage, float weaponRange, targetStateEnum e){
		setAttributes (moveSpeed, health, weaponDamage, weaponRange, e, 1);
	} 
	public void setAttributes(float moveSpeed, float health, float weaponDamage, float weaponRange, targetStateEnum e, int enemyID){
		this.weaponRange = weaponRange;
		this.moveSpeed = moveSpeed;
		currentHealth = maxHealth = health; //TODO make sure this is passing right
		this.weaponDamage = weaponDamage;
		state = e;
		this.enemyId = enemyId;
		updatePath();
		calculateMoveTarget();
	} 





	/*
	//when this enemy takes damage returns true if it died 
	public bool RemoveHealth(int hitAmount){
		health -= hitAmount;
		if(health <= 0){
			deathActions();
			return true;
		}
		return false;
	}
	private void deathActions(){
		//stuff

		Destroy (gameObject);
	}
	*/




	protected void followPath(){
		if(path != null && currentPathIndex < path.Count){
			if (!checkPathClear ()) {
				updatePath ();
			}
			if(moveTarget != transform.position){
				calculateMoveTarget();
			}
			else{
				currentPathIndex++;
				calculateMoveTarget();
			}
			float step = moveSpeed * Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position, moveTarget, step);
		}
	}
	protected  void calculateMoveTarget(){
		moveTarget = Grid.GetWorldPos((int)path[currentPathIndex].x, (int)path[currentPathIndex].y);

		moveTarget = new Vector3(moveTarget.x, transform.position.y, moveTarget.z);
	}
	protected  bool checkPathClear(){
		for (int i = 0; currentPathIndex + i < path.Count && i < pathAheadCheck; i++) {
			if(!pathfinder.checkWalkable((int)path[currentPathIndex + i].x, (int)path[currentPathIndex + i].y)){
				return false;
			}
		}
		return true;
	}
	public void updatePath(){
		switch(state){
		case targetStateEnum.targetPlayer:
			updatePath (GameManager.GetClosestPlayer(transform.position).gameObject);
			break;
		case targetStateEnum.targetBase:
		default:
			updatePath (endBase);
			break;
		}
	}	
	protected void updatePath(GameObject target){
		currentTarget = target;
		try{
		path = pathfinder.findPath (Grid.GetVector2 (transform.position), Grid.GetVector2 (target.transform.position));
		}catch(Exception e){
			Debug.LogError ("The error below is expected. Doesn't seem to affect anything and I decided to move on after trying to fix for a while. If you want to attemp to fix it, first delete this try catch as it moves the error. - Riordan");

		}
		currentPathIndex = 0;
	}
}
